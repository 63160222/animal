/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.animal;

/**
 *
 * @author MSI GAMING
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Animal hh1 = h1;
        System.out.println("hh1 is land animal ? " + (hh1 instanceof LandAnimal));
        System.out.println("hh1 is reptile animal ? " + (hh1 instanceof Reptile));
        System.out.println("hh1 is aquatic animal ? " + (hh1 instanceof AquaticAnimals));
        System.out.println("hh1 is poultry animal ? " + (hh1 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Cat c1 = new Cat("meaw");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));

        Animal cc1 = c1;
        System.out.println("cc1 is land animal ? " + (cc1 instanceof LandAnimal));
        System.out.println("cc1 is reptile animal ? " + (cc1 instanceof Reptile));
        System.out.println("cc1 is aquatic animal ? " + (cc1 instanceof AquaticAnimals));
        System.out.println("cc1 is poultry animal ? " + (cc1 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Dog d1 = new Dog("Cocoa");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("c1 is animal ? " + (d1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (d1 instanceof LandAnimal));

        Animal dd1 = d1;
        System.out.println("dd1 is land animal ? " + (dd1 instanceof LandAnimal));
        System.out.println("dd1 is reptile animal ? " + (dd1 instanceof Reptile));
        System.out.println("dd1 is aquatic animal ? " + (dd1 instanceof AquaticAnimals));
        System.out.println("dd1 is poultry animal ? " + (dd1 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Crocodile ccd1 = new Crocodile("Green");
        ccd1.crawl();
        ccd1.eat();
        ccd1.walk();
        ccd1.speak();
        ccd1.sleep();
        System.out.println("ccd1 is animal ? " + (ccd1 instanceof Animal));
        System.out.println("ccd1 is  reptile animal ? " + (ccd1 instanceof Reptile));

        Animal ccd2 = ccd1;
        System.out.println("ccd2 is land animal ? " + (ccd2 instanceof LandAnimal));
        System.out.println("ccd2 is reptile animal ? " + (ccd2 instanceof Reptile));
        System.out.println("ccd2 is aquatic animal ? " + (ccd2 instanceof AquaticAnimals));
        System.out.println("ccd2 is poultry animal ? " + (ccd2 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Snake s1 = new Snake("Rattlesnake");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is  reptile animal ? " + (s1 instanceof Reptile));

        Animal ss2 = s1;
        System.out.println("ss2 is land animal ? " + (ss2 instanceof LandAnimal));
        System.out.println("ss2 is reptile animal ? " + (ss2 instanceof Reptile));
        System.out.println("ss2 is aquatic animal ? " + (ss2 instanceof AquaticAnimals));
        System.out.println("ss2 is poultry animal ? " + (ss2 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Fish f1 = new Fish("Dolphin");
        f1.swin();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is  aquatic animal ? " + (f1 instanceof AquaticAnimals));

        Animal ff2 = f1;
        System.out.println("ff2 is land animal ? " + (ff2 instanceof LandAnimal));
        System.out.println("ff2 is reptile animal ? " + (ff2 instanceof Reptile));
        System.out.println("ff2 is aquatic animal ? " + (ff2 instanceof AquaticAnimals));
        System.out.println("ff2 is poultry animal ? " + (ff2 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Crab ca1 = new Crab("Nong Pu");
        ca1.swin();
        ca1.eat();
        ca1.walk();
        ca1.speak();
        ca1.sleep();
        System.out.println("ca1 is animal ? " + (ca1 instanceof Animal));
        System.out.println("ca1 is  aquatic animal ? " + (ca1 instanceof AquaticAnimals));

        Animal ca2 = ca1;
        System.out.println("ca2 is land animal ? " + (ca2 instanceof LandAnimal));
        System.out.println("ca2 is reptile animal ? " + (ca2 instanceof Reptile));
        System.out.println("ca2 is aquatic animal ? " + (ca2 instanceof AquaticAnimals));
        System.out.println("ca2 is poultry animal ? " + (ca2 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Bat b1 = new Bat("Covid");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));

        Animal b2 = b1;
        System.out.println("b2 is land animal ? " + (b2 instanceof LandAnimal));
        System.out.println("b2 is reptile animal ? " + (b2 instanceof Reptile));
        System.out.println("b2 is aquatic animal ? " + (b2 instanceof AquaticAnimals));
        System.out.println("b2 is poultry animal ? " + (b2 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");

        Bird bird1 = new Bird("Red");
        bird1.fly();
        bird1.eat();
        bird1.walk();
        bird1.speak();
        bird1.sleep();
        System.out.println("bird1 is animal ? " + (bird1 instanceof Animal));
        System.out.println("bird1 is poultry animal ? " + (bird1 instanceof Poultry));

        Animal  bird2 =  bird1;
        System.out.println("bird2 is land animal ? " + (bird2 instanceof LandAnimal));
        System.out.println("bird2 is reptile animal ? " + (bird2 instanceof Reptile));
        System.out.println("bird2 is aquatic animal ? " + (bird2 instanceof AquaticAnimals));
        System.out.println("bird2 is poultry animal ? " + (bird2 instanceof Poultry));
        System.out.println("--------------------------------------------------------------------");
        
        
        

    }

}
