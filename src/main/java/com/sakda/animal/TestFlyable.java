/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.animal;

/**
 *
 * @author MSI GAMING
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("covid19");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("duck");
        Fish fish = new Fish("Fish");
        Crab crab = new Crab("Crab");
        Crocodile crocodile= new Crocodile("Crocodile");
        Snake snake= new Snake("Snake");
        
        Flyable[] flyable = {bat,plane};
        for(Flyable f : flyable){
            if(f instanceof Plane){
                Plane p =(Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        Runable[] runable = {dog,plane};
        for(Runable r : runable){
            r.run();
        }
        
        Swinable[] swinable = {fish,crab};
        for(Swinable f : swinable){
            f.swin();
        }
        
        Crawlable[] crawlable = {crocodile,snake};
        for(Crawlable c : crawlable){
            c.crawl();
        }
        
    }
}
