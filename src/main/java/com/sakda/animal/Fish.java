/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.animal;

/**
 *
 * @author MSI GAMING
 */
public class Fish extends AquaticAnimals {

    private String nickname;

    public Fish(String nickname) {
        super("Fish");
        this.nickname = nickname;
    }

    

    @Override
    public void eat() {
        System.out.println("Fish: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Fish: " + nickname + " can't walk");
    }

    @Override
    public void speak() {
        System.out.println("Fish: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish: " + nickname + " sleep");
    }

    @Override
    public void swin() {
       System.out.println("Fish: " + nickname + " swin");
    }

}
