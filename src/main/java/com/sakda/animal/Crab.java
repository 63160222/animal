/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.animal;

/**
 *
 * @author MSI GAMING
 */
public class Crab extends AquaticAnimals {

    private String nickname;

    public Crab(String nickname) {
        super("Crab");
        this.nickname = nickname;
    }

   

    @Override
    public void eat() {
        System.out.println("Crab: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + nickname + " sleep");
    }

    @Override
    public void swin() {
       System.out.println("Crab: " + nickname + " swin");
    }

}
